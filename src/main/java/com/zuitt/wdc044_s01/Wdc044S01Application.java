package com.zuitt.wdc044_s01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController // annotation
/*
 * rest controller handles the incoming HTTP request ang generate HTTP responses
 * annotations are optional. they are primarily used to enhance behavior, configuration, or processing of code elements
 */
public class Wdc044S01Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044S01Application.class, args);
	}

	@GetMapping("/hello") // http://localhost:8080/hello
	public String hello(@RequestParam(value = "name",defaultValue = "World") String name) {
		return String.format("Hello %s!",name); // %s will be replaced by the value of name
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user") String user) {
		return String.format("Hi %s!",user);
	}

}
