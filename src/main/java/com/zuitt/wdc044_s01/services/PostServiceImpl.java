package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.config.JwtToken;
import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.models.User;
import com.zuitt.wdc044_s01.repositories.PostRepository;
import com.zuitt.wdc044_s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    @Autowired // @Autowired - automatically injects the dependenciews. it saves you from manually creating and wiring the dependencies allowing Spring to handle the process for you
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    @Override
    public void createPost(String stringToken, Post post) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
    }

    @Override
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
//        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post postForUpdate = postRepository.findById(id).get();
        String postAuthor =postForUpdate.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(postAuthor.equals(authenticatedUser)) {
            postForUpdate.setTitle(post.getTitle());
            postForUpdate.setContent(post.getContent());
            postRepository.save(postForUpdate);
            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        } else {
            return  new ResponseEntity<>("You are not authorized to edit post", HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForDelete = postRepository.findById(id).get();
        String postAuthor =postForDelete.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(postAuthor.equals(authenticatedUser)) {
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        } else {
            return  new ResponseEntity<>("You are not authorized to delete post", HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public Iterable<Post> getMyPosts(String stringToken) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return postRepository.findByUser(author);
    }
}
