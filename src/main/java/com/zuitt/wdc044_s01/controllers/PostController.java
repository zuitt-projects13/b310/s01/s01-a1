package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.JwtResponse;
import com.zuitt.wdc044_s01.services.PostService;
import com.zuitt.wdc044_s01.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    // set endpoint
    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost (@RequestHeader (value = "Authorization") String stringToken, @RequestBody Post post) {
        postService.createPost(stringToken,post);
        return new ResponseEntity<>("Post Created Successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<?> getPosts () {
//        Iterable<Post> posts = postService.getPosts();
//        return new ResponseEntity<>(posts, HttpStatus.OK);

        // shorter version
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @RequestMapping(value="/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        return new ResponseEntity<>(postService.updatePost(postid,stringToken,post), HttpStatus.OK);
    }

    @RequestMapping(value="/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(postService.deletePost(postid,stringToken), HttpStatus.OK);
    }

    @RequestMapping(value="/myPosts", method = RequestMethod.GET)
    public ResponseEntity<?> getMyPosts (@RequestHeader(value = "Authorization") String stringToken) {
        return new ResponseEntity<>(postService.getMyPosts(stringToken), HttpStatus.OK);
    }
}
