package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Object> {
    // The Object in CrudRepository<User, Object> is the type of the ID of the entity that the repository manages. The ID type is used to identify the entity and is used as a primary key in the database. The ID type can be any type that is supported by Spring Data, such as Long, Integer, or String.
    User findByUsername(String username); // custom method. abstract
}
