package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
// By annotating it with @Repository, it becomes a Spring-managed repository component that handles data access operations for the POST entity
public interface PostRepository extends CrudRepository <Post, Object> {
    // by extending CrudRespository, PostRepository has inherited its predefined method for Creating, Retrieving,Updating and deleting records
   Iterable<Post> findByUser(User user);
}